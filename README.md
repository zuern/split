# Split

Split is an expense tracker designed to make splitting expenses between people
easy.

## Features
- Create projects to track sets of expenses (e.g. "Household Expenses") between
  multiple people.
- Assign arbitrary weights to people for repayment (e.g. 50/50 split, 60/40,
  etc.) at a project or a transaction level.
- Settlement view to see how to settle debts.
- Each transaction can include:
  - Description
  - Date
  - Payer & Owers
  - Tags
  - Weights
  - Attachment
  - Custom Metadata

## Future Work

### TODO:

Short term:
- CSRF protection
- Request throttling auth endpoints to prevent brute force
- Improved user UI
- User account screen
- Change password

Someday:
- Email registration confirmation
- User password reset
- User permissions
- Admin password reset / user management.

### Ideas
- Upload receipt scans into a queue. Allows to scan receipt at the store and
  then enter the data at a later point in the future.
- Itemized transactions
- Charting / visualization
- Reporting
- Export to beancount
