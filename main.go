package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"
	"time"

	"gitlab.com/zuern/kit/cli"
	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split"
)

const PFX = "SPLIT_"

var cfg = split.Config{
	DatabaseName: "split",
	DatabaseURL:  "mongodb://localhost:27017",
	ServerAddr:   "0.0.0.0",
	ServerPort:   3000,
	RootURL:      "http://localhost:3000",
}

var (
	version           string
	devLogs, textLogs bool
	logLevel          log.Level = log.InfoLevel
)

func init() {
	version = "unknown"
	var rev, dirty string
	if buildInfo, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range buildInfo.Settings {
			switch setting.Key {
			case "vcs.revision":
				rev = setting.Value
			case "vcs.modified":
				if setting.Value == "true" {
					dirty = " (dirty)"
				}
			}
		}
		if rev != "" {
			version = fmt.Sprintf("revision %s%s", rev, dirty)
		}
	}
}

func main() {
	var exitCode int
	cli.Create(
		"split",
		"Split helps you to easily split expenses between your friends.",
		version,
		[]*cli.Opt{
			{
				Name: "d",
				Desc: "Name of the database to use.",
				Ptr:  &cfg.DatabaseName,
				Env:  PFX + "MONGO_DATABASE_NAME",
			},
			{
				Name: "m",
				Desc: "Mongo connection URI",
				Ptr:  &cfg.DatabaseURL,
				Env:  PFX + "MONGO_CONNECTION_URI",
			},
			{
				Name: "p",
				Desc: "HTTP port to serve on. Set to 0 to choose a random free port.",
				Ptr:  &cfg.ServerPort,
				Env:  PFX + "HTTP_PORT",
			},
			{
				Name: "addr",
				Desc: "Bind address to serve on. 0.0.0.0 serves on all interfaces.",
				Ptr:  &cfg.ServerAddr,
				Env:  PFX + "HTTP_ADDR",
			},
			{
				Name: "rootURL",
				Desc: "" +
					"RootURL is the scheme+host+port of the web application " +
					"(eg https://www.example.com:8080) for url generation. No trailing slash.",
				Ptr:      &cfg.RootURL,
				Env:      PFX + "ROOT_URL",
				TypeName: "url",
			},
			{
				Name: "lvl",
				Desc: fmt.Sprintf("Log level %v", []log.Level{
					log.DebugLevel, log.InfoLevel, log.WarnLevel,
					log.ErrorLevel, log.DPanicLevel, log.PanicLevel, log.FatalLevel,
				}),
				Ptr: &logLevel,
				Env: PFX + "LOG_LEVEL",
			},
			{
				Name: "txt",
				Desc: "Print logs in human readable text format",
				Ptr:  &textLogs,
				Env:  PFX + "TEXT_LOGS",
			},
			{
				Name: "dev",
				Desc: "Development mode logging, NOT SUITABLE FOR PRODUCTION",
				Ptr:  &devLogs,
			},
		},
	)

	logger, err := (&log.Config{
		Development: devLogs,
		Level:       logLevel,
		EncodingConfig: log.EncodingConfig{
			TextFormat: textLogs,
		},
	}).New()
	if err != nil {
		exitCode = 1
		fmt.Fprintf(os.Stderr, "Failed to set up logger: %v\n", err)
		return
	}

	cfg.Logger = logger

	defer func() {
		if r := recover(); r != nil {
			if exitCode == 0 {
				exitCode = 1
			}
			panic(r)
		}
		if exitCode == 0 && err == nil {
			logger.Info("Shut down gracefully. Good bye.")
		}
		os.Exit(exitCode)
	}()

	app, err := split.New(cfg)
	if err != nil {
		exitCode = 1
		logger.Error("Server closed unexpectedly", log.Err(err))
		return
	}

	// Create chan to listen for operating system signals.
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, syscall.SIGINT, syscall.SIGTERM)
	signal := <-osSignals // Block until a shutdown signal is received.

	// Shut down the application.
	const dur = 90 * time.Second
	signames := map[os.Signal]string{
		syscall.SIGINT:  "interrupt",
		syscall.SIGTERM: "terminate",
	}
	signame, ok := signames[signal]
	if !ok {
		signame = signal.String()
	}
	logger.Infof("Received signal %s, shutting down gracefully within %.0f seconds.", signame, dur.Seconds())
	ctx, cf := context.WithTimeout(context.Background(), dur)
	if err = app.Shutdown(ctx); err != nil {
		exitCode = 1
		logger.Error("Didn't gracefully shut down", log.Err(err))
	}
	cf()
}
