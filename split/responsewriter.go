package split

import (
	"net/http"

	"github.com/volatiletech/authboss/v3"
)

// responseWriter records the status code of the response.
type responseWriter struct {
	http.ResponseWriter
	statusCode int
}

// Guarantee that responseWriter implements authboss.UnderlyingResponseWriter.
// The interface needs to be implemented so that authboss can access its
// underlying ClientStateResponseWriter to be able to write cookies for session
// management, otherwise a panic can occur during login / logout.
var _ authboss.UnderlyingResponseWriter = &responseWriter{}

// UnderlyingResponseWriter returns the underlying response writer.
func (r *responseWriter) UnderlyingResponseWriter() http.ResponseWriter {
	return r.ResponseWriter
}

// WriteHeader calls http.ResponseWriter.WriteHeader after recording status
// code.
func (res *responseWriter) WriteHeader(code int) {
	res.statusCode = code
	res.ResponseWriter.WriteHeader(code)
}

// WriteHeader calls http.ResponseWriter.Write after recording status
// code.
func (res *responseWriter) Write(b []byte) (int, error) {
	if res.statusCode == 0 {
		res.statusCode = 200
	}
	return res.ResponseWriter.Write(b)
}
