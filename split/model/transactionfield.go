package model

// TransactionField is a GraphQL enum.
type TransactionField string

func (t TransactionField) String() string {
	return string(t)
}

const (
	TransactionFieldDate        TransactionField = "DATE"
	TransactionFieldDescription TransactionField = "DESCRIPTION"
	TransactionFieldID          TransactionField = "ID"
	TransactionFieldPayer       TransactionField = "PAYER"
	TransactionFieldProject     TransactionField = "PROJECT"
	TransactionFieldTotal       TransactionField = "TOTAL"
)
