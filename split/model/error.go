package model

import (
	"fmt"
)

// Type UserError is used for errors caused by invalid input or other issues
// with business logic. They are intended for the end-user and do not require
// investigation by an administrator.
type Error struct {
	Code    ErrorCode
	Message string
}

func NewError(code ErrorCode, message string) *Error {
	return &Error{
		Code:    code,
		Message: message,
	}
}

func NewErrorf(code ErrorCode, format string, args ...interface{}) *Error {
	return NewError(code, fmt.Sprintf(format, args...))
}

func (e Error) Error() string {
	return e.Message
}

type ErrorCode string

const (
	ERR_API            ErrorCode = "API_ERROR"
	ERR_INVALID_DATA   ErrorCode = "INVALID_DATA"
	ERR_INVALID_FILTER ErrorCode = "INVALID_FILTER"
	ERR_UNSUPPORTED    ErrorCode = "UNSUPPORTED"
	ERR_PAGE_SIZE      ErrorCode = "PAGE_SIZE_TOO_LARGE"
)
