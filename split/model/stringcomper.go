package model

import "fmt"

type StringComper interface {
	comparable
	fmt.Stringer
}
