package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/split/split/model"
)

func TestTransactionFieldString(t *testing.T) {
	tf := model.TransactionFieldDate
	require.Equal(t, string(tf), tf.String())
}
