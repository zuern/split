package model

type UserField string

func (u UserField) String() string {
	return string(u)
}

const (
	UserFieldUsername UserField = "USERNAME"
)
