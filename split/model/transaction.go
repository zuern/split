package model

import "time"

type Transaction struct {
	// ID of the transaction.
	ID string
	// Project this transaction is associated with.
	Project string
	// Date of the transactoin.
	Date time.Time
	// Description (e.g. "Gasoline")
	Description string
	// Notes.
	Notes string
	// Total cost of the transaction.
	Total Money
	// Payer is the person who paid.
	Payer string
	// Owers are the people for whom the transaction was for.
	Owers []string
	// Weights to proportionally calculate how much each person owes. If nonempty
	// Weights must be provided for each Ower. To calculate what's owed, the
	// total is multiplied by the ratio of a person's weight to the sum of the
	// weights. Given [Alice, Bob] and weights [60, 40], Alice owes
	// Total * (60 / (60+40)) = Total * 0.6.
	Weights []float64
	// Tags are optional and can contain only alphanumerics, underscore, and
	// dash. E.g. "vacation_2020".
	Tags []string
	// Link is a link to a receipt or other attachment. Meant for image / PDF.
	Link string
	// Metadata are arbitrary key-value data.
	Metadata map[string]any
}
