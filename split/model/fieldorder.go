package model

import "fmt"

type FieldOrder[T StringComper] struct {
	Field T
	Desc  bool
}

func (f FieldOrder[T]) Valid() error {
	var empty T
	if f.Field == empty {
		return fmt.Errorf("field cannot be empty")
	}
	return nil
}
