package model

// ProjectField is a GraphQL enum.
type ProjectField string

func (p ProjectField) String() string {
	return string(p)
}

const (
	ProjectFieldID   ProjectField = "ID"
	ProjectFieldName ProjectField = "NAME"
)
