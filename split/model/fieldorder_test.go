package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/split/split/model"
)

func TestFieldOrderValid(t *testing.T) {
	require := require.New(t)
	order := model.FieldOrder[model.ProjectField]{}
	require.Error(order.Valid())

	order = model.FieldOrder[model.ProjectField]{
		Field: model.ProjectFieldID,
	}
	require.NoError(order.Valid())
}
