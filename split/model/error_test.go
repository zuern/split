package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/split/split/model"
)

// assert it implements the interface.
var _ error = &model.Error{}

func TestError(t *testing.T) {
	require := require.New(t)
	err := model.NewErrorf(model.ERR_API, "test %s", "error")
	require.Equal("test error", err.Error())
	require.Equal("test error", err.Message)
	require.Equal(model.ERR_API, err.Code)
}
