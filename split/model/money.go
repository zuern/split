package model

type Money struct {
	// Currency is an ISO-4217 alpha code e.g. "USD" or "CAD".
	Currency string
	// Millicents is the quantity of money in thousands of the smallest unit of
	// the currency. For CAD, it is 1/1000th of a cent.
	Millicents int
}

func NewMoney(currency string, dollars, cents int) Money {
	return Money{
		Currency:   currency,
		Millicents: 100_000*dollars + 1000*cents,
	}
}

const (
	CAD = "CAD"
	USD = "USD"
)
