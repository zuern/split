package model

type Project struct {
	ID          string
	Name        string
	Description string
	// People is a list of people who are part of the project.
	People []string
	// Weights if provided are default weightings for each person.
	Weights []float32
}
