package model

type User struct {
	ID string
	// Password in hashed and salted form.
	Password string

	// Display Name of the user.
	Name string
}
