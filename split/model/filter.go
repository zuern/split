package model

import (
	"fmt"
)

type Filter[T StringComper] struct {
	And  []Filter[T]
	Or   []Filter[T]
	Keys map[T]FilterValue `mapstructure:",remain"`
}

type FilterValue struct {
	EQ any `mapstructure:"eq"`
}

// Valid returns true if the current filter only is valid. It does NOT consider
// child filters, this must be done externally.
func (f Filter[T]) Valid() (err error) {
	defer func() {
		if err != nil {
			err = NewError(ERR_INVALID_FILTER, err.Error())
		}
	}()
	var children []Filter[T]
	for _, op := range [][]Filter[T]{f.Or, f.And} {
		if len(op) > 0 {
			if children != nil {
				return fmt.Errorf("only one of {AND, OR} can be used in a filter")
			} else {
				children = op
			}
		}
	}
	for _, child := range children {
		if err = child.Valid(); err != nil {
			return err
		}
	}
	if len(children) > 0 {
		return
	}
	// TODO is there a way to validate keys against a type via reflection?
	if len(f.Keys) < 1 {
		return fmt.Errorf("filter cannot be empty")
	}
	for k, v := range f.Keys {
		if v.EQ == nil {
			return fmt.Errorf("key %s must have a comparison operator and value", k)
		}
	}
	return nil
}
