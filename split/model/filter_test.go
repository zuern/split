package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/split/split/model"
)

func TestFilterInvalidEmpty(t *testing.T) {
	var filter model.Filter[model.ProjectField]
	err := filter.Valid()
	require := require.New(t)
	require.Error(err)
	require.Equal("filter cannot be empty", err.Error())
}

func TestFilterInvalidNoComparator(t *testing.T) {
	filter := model.Filter[model.ProjectField]{
		Keys: map[model.ProjectField]model.FilterValue{
			model.ProjectFieldID: {
				// No comparator (e.g. EQ)
			},
		},
	}
	err := filter.Valid()
	require := require.New(t)
	require.Error(err)
	require.Equal("key ID must have a comparison operator and value", err.Error())
}

func TestFilterInvalidMultipleLogicalOperators(t *testing.T) {
	subFilter := model.Filter[model.ProjectField]{
		Keys: map[model.ProjectField]model.FilterValue{
			model.ProjectFieldID: {
				EQ: "123",
			},
		},
	}
	filter := model.Filter[model.ProjectField]{
		And: []model.Filter[model.ProjectField]{subFilter},
		Or:  []model.Filter[model.ProjectField]{subFilter},
	}
	err := filter.Valid()
	require := require.New(t)
	require.Error(err)
	require.Equal("only one of {AND, OR} can be used in a filter", err.Error())
}

func TestFilterInvalidChildIsInvalid(t *testing.T) {
	invalidChild := model.Filter[model.ProjectField]{}
	filter := model.Filter[model.ProjectField]{
		And: []model.Filter[model.ProjectField]{invalidChild},
	}
	err := filter.Valid()
	require := require.New(t)
	require.Error(err)
	require.Equal("filter cannot be empty", err.Error())
}

func TestFilterValidLogical(t *testing.T) {
	subFilter := model.Filter[model.ProjectField]{
		Keys: map[model.ProjectField]model.FilterValue{
			model.ProjectFieldID: {
				EQ: "123",
			},
		},
	}
	filter := model.Filter[model.ProjectField]{
		And: []model.Filter[model.ProjectField]{subFilter},
	}
	require.NoError(t, filter.Valid())
}
