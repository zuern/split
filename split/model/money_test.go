package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/split/split/model"
)

func TestMoney(t *testing.T) {
	// 9.2 is a number which cannot be represented accurately in base 2, similar
	// to how 1/3 cannot be represented accurately as a decimal number
	// (1.33333333333333333333). The Money type must be able to accurately
	// represent values without being impacted by precision issues, so it should
	// be able to accurately represent $9.20 CAD.

	money := model.NewMoney(model.CAD, 9, 20)
	require.Equal(t, model.Money{
		Currency:   model.CAD,
		Millicents: 920_000,
	}, money)
}
