package model_test

import (
	"testing"

	"gitlab.com/zuern/split/split/model"
)

func TestUserFieldToString(t *testing.T) {
	s := model.UserFieldUsername.String()
	if s != "USERNAME" {
		t.FailNow()
	}
}
