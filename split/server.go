package split

import (
	"context"
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/uhn/ggql/pkg/ggql"
	abclientstate "github.com/volatiletech/authboss-clientstate"
	abrenderer "github.com/volatiletech/authboss-renderer"
	"github.com/volatiletech/authboss/v3"
	"github.com/volatiletech/authboss/v3/defaults"
	"gitlab.com/zuern/graphgo"
	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/api"
	"gitlab.com/zuern/split/split/auth"
	"gitlab.com/zuern/split/split/store"
	"gitlab.com/zuern/split/split/util"

	// Import to enable modules.
	_ "github.com/volatiletech/authboss/v3/auth"
	_ "github.com/volatiletech/authboss/v3/logout"
	_ "github.com/volatiletech/authboss/v3/register"
)

//go:generate sh -c "cd frontend && make build"

//go:embed api/schema
var schema embed.FS

//go:embed frontend/build
var frontend embed.FS

//go:embed templates/index.html.gotmpl
var indexHTML []byte

type Server struct {
	http.Server
	graphql     *graphgo.Service
	graphqlDocs string

	store store.Store
	auth  *authboss.Authboss
	log   log.Logger

	// indexTmpl html template for the react SPA html.
	indexTmpl *template.Template
	// jsBundlePath in frontend FS where main app bundle is located.
	jsBundlePath string
	// rootURL where the application is being served.
	rootURL string
}

func NewServer(store store.Store, logger log.Logger, rootURL string) (*Server, error) {
	s := &Server{
		store:     store,
		log:       logger,
		indexTmpl: template.Must(template.New("index").Parse(string(indexHTML))),
		rootURL:   rootURL,
	}

	fe, err := fs.Sub(frontend, "frontend/build")
	if err != nil {
		return nil, fmt.Errorf("re-root frontend FS: %w", err)
	}

	// Find the JS bundle to serve the React frontend with.
	const glob = "static/js/main.*.js"
	if matches, _ := fs.Glob(fe, glob); len(matches) == 0 {
		// This condition will only happen if the react assets weren't built
		// correctly into the binary. Should never happen at runtime and is not a
		// recoverable condition for the server.
		panic(fmt.Sprintf("no main.*.js bundle found with glob %q, did you need to run go generate ./...?", glob))
	} else if len(matches) > 1 {
		panic(fmt.Sprintf("multiple main.*.js bundles found with glob %q, expected only one", glob))
	} else {
		s.jsBundlePath = matches[0]
	}

	// Setup GraphQL API.
	ggql.Sort = true
	root := &api.Schema{Query: &api.Query{}, Mutation: &api.Mutation{}}
	s.graphql = graphgo.NewService(root)
	if err := s.graphql.Root.AddTypes(api.NewDate()); err != nil {
		return nil, fmt.Errorf("add Date scalar: %w", err)
	}
	if err := s.graphql.ParseSchemaFS(schema, "api/schema/*.graphql"); err != nil {
		return nil, fmt.Errorf("parse GraphQL schema: %w", err)
	}
	s.graphqlDocs = strings.TrimSpace(s.graphql.SDL(false, true))

	// Set up auth using no path prefix, routes for login, registration, logout
	// will be at root level. E.g. "/login"
	const authPathPrefix = ""
	if ab, err := s.setupAuth(authPathPrefix, rootURL); err == nil {
		s.auth = ab
	} else {
		return nil, fmt.Errorf("setup auth: %w", err)
	}

	if err := s.setupRoutes(authPathPrefix, fe, rootURL); err != nil {
		return nil, err
	}

	return s, nil
}

func (s *Server) setupRoutes(authPathPrefix string, frontend fs.FS, rootURL string) error {
	const (
		GET     = http.MethodGet
		POST    = http.MethodPost
		OPTIONS = http.MethodOptions
	)
	router := mux.NewRouter()
	router.Use(s.auth.LoadClientStateMiddleware, s.addContextMiddleware, s.logResponseMiddleware)
	router.Path("/api/graphql").Methods(GET, POST, OPTIONS).Handler(s.graphql)
	router.Path("/api/graphql/schema").Methods(GET).HandlerFunc(s.handleGraphQLDocs)

	dashboardR := router.PathPrefix("/dashboard").Subrouter()
	dashboardR.Path("").HandlerFunc(s.handleIndex)

	router.Path("/").HandlerFunc(s.handleIndex)
	router.Path("/about").HandlerFunc(s.handleIndex)
	router.PathPrefix("/static").Handler(newFrontendHandler(http.FS(frontend)))

	router.PathPrefix(authPathPrefix).Handler(http.StripPrefix(
		s.auth.Config.Paths.Mount,
		s.auth.Config.Core.Router,
	))

	dashboardR.Use(authboss.Middleware2(s.auth, authboss.RequireNone, authboss.RespondRedirect))

	s.Server.Handler = router
	return nil
}

func (s *Server) setupAuth(pathPrefix string, rootURL string) (*authboss.Authboss, error) {
	authSecrets, err := s.getOrCreateAuthSecrets()
	if err != nil {
		return nil, err
	}

	ab := authboss.New()
	const sessionCookieName = "session"

	ab.Config.Storage.Server = &auth.ServerStorer{Store: s.store}
	ab.Config.Storage.CookieState = abclientstate.NewCookieStorer(authSecrets.CookieHashKey, authSecrets.CookieBlockKey)
	ab.Config.Storage.SessionState = abclientstate.NewSessionStorer(sessionCookieName, authSecrets.SessionHashKey, authSecrets.SessionBlockKey)

	ab.Config.Paths.RootURL = rootURL
	ab.Config.Paths.Mount = pathPrefix

	// This is using the renderer from: github.com/volatiletech/authboss
	ab.Config.Core.ViewRenderer = abrenderer.NewHTML(pathPrefix, "ab_views")

	defaults.SetCore(&ab.Config, false, false)

	// The preserve fields are things we don't want to lose when we're doing user
	// registration (prevents having to type them again)
	ab.Config.Modules.RegisterPreserveFields = []string{auth.FieldName, auth.FieldEmail}

	// Require a POST request to log out.
	ab.Config.Modules.LogoutMethod = http.MethodPost

	const REGISTER = "register"
	const PASSWORD = "password"

	emailRule := defaults.Rules{
		FieldName: auth.FieldEmail, Required: true,
		MatchError: "Must be a valid email address",
		MustMatch:  regexp.MustCompile(`.*@.*`),
	}
	passwordRule := defaults.Rules{
		FieldName: PASSWORD, Required: true,
		MinLength: 1, // TODO change.
	}
	nameRule := defaults.Rules{
		FieldName: auth.FieldName, Required: true,
		MinLength:       1,
		AllowWhitespace: true,
	}
	ab.Config.Core.BodyReader = defaults.HTTPBodyReader{
		Rulesets: map[string][]defaults.Rules{
			REGISTER: {emailRule, passwordRule, nameRule},
		},
		Confirms: map[string][]string{
			REGISTER: {PASSWORD, authboss.ConfirmPrefix + PASSWORD},
		},
		Whitelist: map[string][]string{
			REGISTER: {auth.FieldEmail, auth.FieldName, PASSWORD},
		},
	}

	ab.Config.Core.Logger = &auth.Logger{Log: s.log}

	if err := ab.Init(); err != nil {
		return nil, err
	}

	return ab, nil
}

func (s *Server) getOrCreateAuthSecrets() (*authSecrets, error) {
	const (
		configKey   = "app_auth_secrets"
		hashKeyLen  = 64 // Recommended by securecookie.New docs
		blockKeyLen = 32 // Uses AES-256 for encryption.
	)
	ctx, cf := context.WithTimeout(context.Background(), 3*time.Second)
	defer cf()
	secrets, err := s.store.GetConfig(ctx, configKey)
	if err != nil {
		return nil, fmt.Errorf("lookup auth secrets: %w", err)
	}

	var authSecrets authSecrets
	if secrets != "" {
		if err = json.Unmarshal([]byte(secrets), &authSecrets); err != nil {
			return nil, fmt.Errorf("unmarshal auth secrets: %w", err)
		}
		return &authSecrets, nil
	}

	// No secrets set up, generate new ones.

	newKey := func(length int) ([]byte, error) {
		var k []byte
		for i := 0; i < 3; i++ {
			k = securecookie.GenerateRandomKey(length)
			if k != nil {
				break
			}
		}
		if k == nil {
			return nil, fmt.Errorf("failed to generate new auth secrets, system random number generator failed")
		}
		return k, nil
	}

	authSecrets.CookieHashKey, err = newKey(hashKeyLen)
	if err == nil {
		authSecrets.CookieBlockKey, err = newKey(blockKeyLen)
	}
	if err == nil {
		authSecrets.SessionHashKey, err = newKey(hashKeyLen)
	}
	if err == nil {
		authSecrets.SessionBlockKey, err = newKey(blockKeyLen)
	}
	if err == nil {
		if err = s.store.SetConfig(ctx, configKey, util.ToJSON(authSecrets)); err != nil {
			err = fmt.Errorf("failed to persist auth secrets: %w", err)
		}
	}
	return &authSecrets, err
}

func (s *Server) handleGraphQLDocs(res http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(res, s.graphqlDocs)
}

// handleIndex serves the index.html page used for all React pages.
func (s *Server) handleIndex(res http.ResponseWriter, req *http.Request) {
	var html template.HTML

	// TODO remove after testing. Demonstrating logged in vs. not logged in state
	// and making it easier to test.
	if u, err := s.currentUser(req); err == nil && u != nil {
		html = template.HTML(fmt.Sprintf(`  <p>Hello there %s, how are you?</p>
  <br>
  <form method="post" action="/logout">
    <button type="submit">Log out</button>
  </form>
  `, u.Name))
	} else {
		html = template.HTML("  <p>Click <a href=\"/login\">here</a> to log in.</p>\n  ")
	}

	if err := s.indexTmpl.Execute(res, map[string]any{
		"body_end":   html,
		"public_url": s.rootURL,
		"js_bundle":  s.jsBundlePath,
	}); err != nil {
		api.FromContext(req.Context()).Log.Error("render index.html template failed", log.Err(err))
		http.Error(res, "http 500: internal server error", http.StatusInternalServerError)
	}
}

func (s *Server) currentUser(req *http.Request) (*auth.User, error) {
	u, err := s.auth.CurrentUser(req)
	if err == nil {
		user := u.(*auth.User)
		return user, nil
	} else if errors.Is(err, authboss.ErrUserNotFound) {
		return nil, nil
	} else {
		return nil, fmt.Errorf("failed to look up user: %w", err)
	}
}
