import { useIntl } from "react-intl";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

export default function App() {
  const intl = useIntl();
  const paths = [
    {
      text: intl.formatMessage({ defaultMessage: 'Home', id: 'home', }),
      path: '/',
      component: Home,
    },
    {
      text: intl.formatMessage({ defaultMessage: 'About', id: 'about', }),
      path: '/about',
      component: About,
    },
    {
      text: intl.formatMessage({ defaultMessage: 'Dashboard', id: 'dashboard', }),
      path: '/dashboard',
      component: Dashboard,
    }
  ];
  return (
    <Router>
      <div>
        <ul>
        {paths.map((p) => (
          <li key={p.path}>
            {/*
            Could use Link here to route without reloading but we want a full
            page load to allow auth to be handled from the backend.
            */}
            <a href={p.path}>{p.text}</a>
          </li>
        ))}
        </ul>

        <hr />

        <Routes>
          {paths.map((p) => (
            <Route
              key={p.path}
              path={p.path}
              element={(p.component())}
            />
          ))}
        </Routes>
      </div>
    </Router>
  );
}

const Home = (): JSX.Element => (
  <div>
    <h2>Home</h2>
  </div>
);

const About = (): JSX.Element => {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}

const Dashboard = (): JSX.Element => {
  return (
    <div>
      <h2>Dashboard</h2>
    </div>
  );
}

