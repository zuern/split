package split

type authSecrets struct {
	CookieHashKey   []byte `json:"cookieHashKey"`
	CookieBlockKey  []byte `json:"cookieBlockKey"`
	SessionHashKey  []byte `json:"sessionHashKey"`
	SessionBlockKey []byte `json:"sessionBlockKey"`
}
