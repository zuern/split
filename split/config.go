package split

import "gitlab.com/zuern/kit/log"

type Config struct {
	// DatabaseName name of the database.
	DatabaseName string
	// DatabaseURL MongoDB Connection URI.
	DatabaseURL string
	// ServerAddr bind address for the HTTP server.
	ServerAddr string
	// ServerPort to serve on.
	ServerPort int
	// RootURL is the scheme+host+port of the web application (e.g.
	// https://www.example.com:8080 for url generation. No trailing slash.
	RootURL string
	Logger  log.Logger
}
