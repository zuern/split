package split

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"

	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/model"
	"gitlab.com/zuern/split/split/store"
)

type Split struct {
	store  store.Store
	server *Server
}

func New(cfg Config) (split *Split, err error) {
	logger := cfg.Logger
	logger.Info("Connecting to mongo")
	cx, cf := context.WithTimeout(context.Background(), 3*time.Second)
	store, err := store.NewMongoStore(cx, cfg.DatabaseURL, cfg.DatabaseName, 0)
	if err != nil {
		logger.Error("Setup mongo store failed", log.Err(err))
		cf()
		return
	}
	cf()

	// TODO remove.
	if _, err = store.UpsertProject(
		context.Background(), model.Project{
			ID:      "home",
			Name:    "Household Expenses",
			People:  []string{"Alice", "Bob"},
			Weights: []float32{60, 40},
		},
	); err != nil {
		panic(err)
	}

	server, err := NewServer(store, logger, cfg.RootURL)
	if err != nil {
		logger.Error("Set up server failed", log.Err(err))
		return
	}

	addr := fmt.Sprintf("%s:%d", cfg.ServerAddr, cfg.ServerPort)
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}
	go func() {
		addr = ln.Addr().String() // update in case random port was assigned.
		logger.Info(fmt.Sprintf("Serving on http://%s", addr))
		if err = server.Serve(ln); err != nil && errors.Is(err, http.ErrServerClosed) {
			err = nil
		}
		if err != nil {
			logger.Fatal("Server error", log.Err(err))
		}
	}()

	split = &Split{
		store:  store,
		server: server,
	}

	return
}

func (s *Split) Shutdown(ctx context.Context) error {
	return s.store.Disconnect(ctx)
}
