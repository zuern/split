package store

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/zuern/split/split/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoStore struct {
	mc          *mongo.Client
	db          *mongo.Database
	maxPageSize int
}

// NewMongoStore creates a new mongo store and connects to the server.
func NewMongoStore(
	cx context.Context,
	mongoURL, databaseName string,
	maxPageSize int,
) (*MongoStore, error) {
	mc, err := mongo.Connect(cx, (&options.ClientOptions{}).ApplyURI(mongoURL))
	if err != nil {
		return nil, err
	}
	if maxPageSize == 0 {
		maxPageSize = defaultMaxPageSize
	}
	// TODO add indexes, add unique constraint for User.username
	return &MongoStore{
		mc:          mc,
		db:          mc.Database(databaseName),
		maxPageSize: maxPageSize,
	}, nil
}

var allCollections = []string{
	colConfig,
	colProject,
	colTransaction,
	colUser,
}

const (
	// New collections need to go in allCollections
	colConfig      = "Config"
	colProject     = "Project"
	colTransaction = "Transaction"
	colUser        = "User"

	defaultMaxPageSize = 500
)

func (m *MongoStore) GetTransaction(ctx context.Context, id string) (*model.Transaction, error) {
	var txn *model.Transaction
	if err := m.get(ctx, colTransaction, id, &txn); err != nil {
		return nil, err
	}
	return txn, nil
}

func (m *MongoStore) UpsertTransaction(ctx context.Context, t model.Transaction) (*model.Transaction, error) {
	if t.ID == "" {
		t.ID = uuid.NewString()
	}
	if err := m.upsert(ctx, colTransaction, t.ID, t); err != nil {
		return nil, err
	}
	return &t, nil
}

func (m *MongoStore) DeleteTransaction(ctx context.Context, id string) error {
	return m.delete(ctx, colTransaction, id)
}

func (m *MongoStore) FindTransactions(
	ctx context.Context,
	filter *model.Filter[model.TransactionField],
	orderBy []model.FieldOrder[model.TransactionField],
	pageSize, offset int,
) ([]model.Transaction, error) {
	if filter != nil {
		if err := filter.Valid(); err != nil {
			return nil, err
		}
	}
	for _, o := range orderBy {
		if err := o.Valid(); err != nil {
			return nil, err
		}
	}
	order, err := transformOrderBy(orderBy, transactionFieldMap)
	if err != nil {
		return nil, err
	}
	tFilter, err := transformFilter(filter, transactionFieldMap)
	if err != nil {
		return nil, err
	}
	page, err := m.find(
		ctx,
		colTransaction,
		tFilter,
		order,
		pageSize, offset,
		func() any {
			return &model.Transaction{}
		},
	)
	if err != nil {
		return nil, err
	}
	var txns []model.Transaction
	for _, t := range page {
		txns = append(txns, *(t.(*model.Transaction)))
	}
	return txns, nil
}

func (m *MongoStore) UpsertProject(ctx context.Context, p model.Project) (*model.Project, error) {
	if p.ID == "" {
		p.ID = uuid.NewString()
	}
	if err := m.upsert(ctx, colProject, p.ID, p); err != nil {
		return nil, err
	}
	return &p, nil
}

func (m *MongoStore) GetProject(ctx context.Context, id string) (*model.Project, error) {
	var project *model.Project
	if err := m.get(ctx, colProject, id, &project); err != nil {
		return nil, err
	}
	return project, nil
}

func (m *MongoStore) DeleteProject(ctx context.Context, id string) error {
	return m.delete(ctx, colProject, id)
}

func (m *MongoStore) FindProjects(
	ctx context.Context,
	filter *model.Filter[model.ProjectField],
	orderBy []model.FieldOrder[model.ProjectField],
	pageSize, offset int,
) ([]model.Project, error) {
	if filter != nil {
		if err := filter.Valid(); err != nil {
			return nil, err
		}
	}
	for _, o := range orderBy {
		if err := o.Valid(); err != nil {
			return nil, err
		}
	}
	order, err := transformOrderBy(orderBy, projectFieldMap)
	if err != nil {
		return nil, err
	}
	tFilter, err := transformFilter(filter, projectFieldMap)
	if err != nil {
		return nil, err
	}
	page, err := m.find(
		ctx,
		colProject,
		tFilter,
		order,
		pageSize, offset,
		func() any {
			return &model.Project{}
		},
	)
	if err != nil {
		return nil, err
	}
	var projects []model.Project
	for _, p := range page {
		projects = append(projects, *(p.(*model.Project)))
	}
	return projects, nil
}

func (m *MongoStore) UpsertUser(ctx context.Context, u model.User) (*model.User, error) {
	if u.ID == "" {
		return nil, model.NewErrorf(model.ERR_INVALID_DATA, "create user: id cannot be empty")
	}
	if err := m.upsert(ctx, colUser, u.ID, u); err != nil {
		return nil, err
	}
	return &u, nil
}

func (m *MongoStore) GetUser(ctx context.Context, id string) (*model.User, error) {
	var user *model.User
	if err := m.get(ctx, colUser, id, &user); err != nil {
		return nil, err
	}
	return user, nil
}

func (m *MongoStore) DeleteUser(ctx context.Context, id string) error {
	return m.delete(ctx, colUser, id)
}

func (m *MongoStore) FindUsers(
	ctx context.Context,
	filter *model.Filter[model.UserField],
	orderBy []model.FieldOrder[model.UserField],
	pageSize, offset int,
) ([]model.User, error) {
	if filter != nil {
		if err := filter.Valid(); err != nil {
			return nil, err
		}
	}
	for _, o := range orderBy {
		if err := o.Valid(); err != nil {
			return nil, err
		}
	}
	order, err := transformOrderBy(orderBy, userFieldMap)
	if err != nil {
		return nil, err
	}
	tFilter, err := transformFilter(filter, userFieldMap)
	if err != nil {
		return nil, err
	}
	page, err := m.find(
		ctx,
		colUser,
		tFilter,
		order,
		pageSize, offset,
		func() any {
			return &model.User{}
		},
	)
	if err != nil {
		return nil, err
	}
	var users []model.User
	for _, p := range page {
		users = append(users, *(p.(*model.User)))
	}
	return users, nil
}

func (m *MongoStore) GetConfig(ctx context.Context, key string) (value string, err error) {
	var data map[string]string
	if err = m.get(ctx, colConfig, key, &data); err == nil {
		value, _ = data[key]
	}
	return value, err
}

func (m *MongoStore) SetConfig(ctx context.Context, key, value string) error {
	return m.upsert(ctx, colConfig, key, map[string]string{key: value})
}

func (m *MongoStore) DropAll(ctx context.Context) error {
	var errs []error
	for _, col := range allCollections {
		if err := m.db.Collection(col).Drop(ctx); err != nil {
			errs = append(errs, fmt.Errorf("drop collection %s: %w", col, err))
		}
	}
	if n := len(errs); n > 0 {
		return fmt.Errorf("drop all collections %d errors occurred: %v", n, errs)
	}
	return nil
}

func (m *MongoStore) Disconnect(ctx context.Context) error {
	return m.mc.Disconnect(ctx)
}

func (m *MongoStore) upsert(ctx context.Context, collection, id string, doc any) error {
	if _, err := m.db.Collection(collection).UpdateOne(
		ctx,
		map[string]any{"id": id},
		bson.M{"$set": doc},
		options.Update().SetUpsert(true),
	); err != nil {
		return fmt.Errorf("upsert %s: %w", collection, err)
	}
	return nil
}

func (m *MongoStore) get(ctx context.Context, collection, id string, obj any) error {
	res := m.db.Collection(collection).FindOne(
		ctx,
		map[string]any{"id": id},
	)
	err := res.Err()
	if errors.Is(mongo.ErrNoDocuments, err) {
		return nil
	}
	if err == nil {
		err = res.Decode(obj)
	}
	if err != nil {
		err = fmt.Errorf("get %s: %w", collection, err)
	}
	return err
}

func (m *MongoStore) delete(ctx context.Context, collection, id string) error {
	if _, err := m.db.Collection(collection).DeleteOne(
		ctx,
		map[string]any{"id": id},
		nil,
	); err != nil {
		return fmt.Errorf("delete %s: %w", collection, err)
	}
	return nil
}

func (m *MongoStore) find(
	ctx context.Context,
	collection string,
	filter, sort any,
	pageSize, offset int,
	newObj func() any,
) (results []any, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("find %s: %w", collection, err)
		}
	}()
	if pageSize > m.maxPageSize {
		return nil, model.NewErrorf(model.ERR_PAGE_SIZE, "page size greater than max of %d", m.maxPageSize)
	}
	cur, err := m.db.Collection(collection).Find(
		ctx,
		filter,
		options.Find().
			SetSort(sort).
			SetLimit(int64(pageSize)).
			SetSkip(int64(offset)),
	)
	if err != nil {
		return nil, err
	}
	defer func() {
		e := cur.Close(ctx)
		if err == nil {
			err = e
		}
	}()
	for cur.Next(ctx) {
		if err = cur.Err(); err != nil {
			return results, err
		}
		obj := newObj()
		if err = cur.Decode(obj); err != nil {
			return results, err
		}
		results = append(results, obj)
	}
	return results, err
}
