package store

import (
	"gitlab.com/zuern/split/split/model"
	"go.mongodb.org/mongo-driver/bson"
)

// transformTransactionFilter converts to a MongoDB-compatible BSON filter.
func transformFilter[T model.StringComper](f *model.Filter[T], keyMap map[T]string) (*bson.M, error) {
	filter := &bson.M{}
	if f == nil {
		return filter, nil
	}

	var childFilters []model.Filter[T]
	var key string
	if len(f.And) > 0 {
		childFilters = f.And
		key = "$and"
	} else if len(f.Or) > 0 {
		childFilters = f.Or
		key = "$or"
	}
	var children []*bson.M
	for _, child := range childFilters {
		if subFilter, err := transformFilter(&child, keyMap); err != nil {
			return nil, err
		} else {
			children = append(children, subFilter)
		}
	}
	if len(children) > 0 {
		(*filter)[key] = children
		return filter, nil
	}

	for key, fv := range f.Keys {
		var val any
		for _, val = range []any{fv.EQ} {
			if val != nil {
				break
			}
		}
		k, have := keyMap[key]
		if !have {
			return nil, model.NewErrorf(model.ERR_UNSUPPORTED, "Filter key %q is not supported", key)
		}
		(*filter)[string(k)] = val
	}

	return filter, nil
}

// transformOrderBy converts to a MongoDB-compatible BSON sort.
func transformOrderBy[T model.StringComper](orderBy []model.FieldOrder[T], keyMap map[T]string) (any, error) {
	var orders bson.D
	for _, o := range orderBy {
		key, have := keyMap[o.Field]
		if !have {
			return orders, model.NewErrorf(model.ERR_UNSUPPORTED, "Field %q in orderBy is not supported", o.Field)
		}
		val := 1
		if o.Desc {
			val = -1
		}
		orders = append(orders, bson.E{Key: key, Value: val})
	}
	if len(orders) == 0 {
		return nil, nil
	}
	return orders, nil
}
