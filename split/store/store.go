package store

import (
	"context"

	"gitlab.com/zuern/split/split/model"
)

type Store interface {
	GetTransaction(ctx context.Context, id string) (*model.Transaction, error)
	UpsertTransaction(ctx context.Context, t model.Transaction) (*model.Transaction, error)
	DeleteTransaction(ctx context.Context, id string) error
	FindTransactions(
		ctx context.Context,
		filter *model.Filter[model.TransactionField],
		orderBy []model.FieldOrder[model.TransactionField],
		pageSize, offset int,
	) ([]model.Transaction, error)

	GetProject(ctx context.Context, id string) (*model.Project, error)
	UpsertProject(ctx context.Context, p model.Project) (*model.Project, error)
	DeleteProject(ctx context.Context, id string) error
	FindProjects(
		ctx context.Context,
		filter *model.Filter[model.ProjectField],
		orderBy []model.FieldOrder[model.ProjectField],
		pageSize, offset int,
	) ([]model.Project, error)

	GetUser(ctx context.Context, id string) (*model.User, error)
	UpsertUser(ctx context.Context, u model.User) (*model.User, error)
	DeleteUser(ctx context.Context, id string) error
	FindUsers(
		ctx context.Context,
		filter *model.Filter[model.UserField],
		orderBy []model.FieldOrder[model.UserField],
		pageSize, offset int,
	) ([]model.User, error)

	GetConfig(ctx context.Context, key string) (string, error)
	SetConfig(ctx context.Context, key, value string) error

	Disconnect(ctx context.Context) error
	DropAll(ctx context.Context) error
}
