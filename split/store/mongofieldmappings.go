package store

import "gitlab.com/zuern/split/split/model"

// projectFieldMap maps from model.ProjectField name to the key as stored in
// MongoDB.
var projectFieldMap = map[model.ProjectField]string{
	model.ProjectFieldID:   "id",
	model.ProjectFieldName: "name",
}

// transactionFieldMap maps from model.TransactionField name to the key as stored
// in MongoDB.
var transactionFieldMap = map[model.TransactionField]string{
	model.TransactionFieldID:          "id",
	model.TransactionFieldProject:     "project",
	model.TransactionFieldDate:        "date",
	model.TransactionFieldDescription: "description",
	model.TransactionFieldTotal:       "total",
	model.TransactionFieldPayer:       "payer",
}

var userFieldMap = map[model.UserField]string{
	model.UserFieldUsername: "username",
}
