package store_test

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/kit/fixture"
	"gitlab.com/zuern/split/split/model"
	"gitlab.com/zuern/split/split/store"
)

func testStoreCRUD(require *require.Assertions, st store.Store) {
	ctx, cf := context.WithTimeout(context.Background(), 5*time.Second)
	defer cf()

	err := st.DropAll(ctx)
	require.NoError(err)

	// Create Project
	proj, err := st.UpsertProject(ctx, model.Project{
		Name:        "household Expenses",
		Description: "tracker for home",
		People:      []string{"alice", "Bob"},
		Weights:     []float32{61, 40},
	})
	require.NoError(err)
	require.NotEmpty(proj.ID)
	id := proj.ID

	// Update Project
	proj, err = st.UpsertProject(ctx, model.Project{
		ID:          proj.ID,
		Name:        "Household Expenses",
		Description: "Tracker for Home.",
		People:      []string{"Alice", "Bob"},
		Weights:     []float32{60, 40},
	})
	require.NoError(err)
	require.Equal(id, proj.ID)
	require.Equal("Household Expenses", proj.Name)
	require.Equal("Tracker for Home.", proj.Description)
	require.Equal([]string{"Alice", "Bob"}, proj.People)
	require.Equal([]float32{60, 40}, proj.Weights)

	// Get Project
	proj = nil
	proj, err = st.GetProject(ctx, "not found")
	require.NoError(err)
	require.Nil(proj)
	proj, err = st.GetProject(ctx, id)
	require.NoError(err)
	require.NotNil(proj)
	require.Equal(id, proj.ID)

	// Delete Project
	err = st.DeleteProject(ctx, id)
	require.NoError(err)
	proj = nil
	proj, err = st.GetProject(ctx, "not found")
	require.NoError(err)
	require.Nil(proj)

	// Create Transaction
	txn, err := st.UpsertTransaction(ctx, model.Transaction{
		Project:     id,
		Date:        time.Date(2022, 02, 20, 0, 0, 0, 0, time.UTC),
		Description: "Groceries",
		Notes:       "Lettuce, Tomatoes, Onions",
		Total:       model.NewMoney(model.CAD, 14, 38),
		Payer:       "Alice",
		Owers:       []string{"Alice", "Bob"},
		Weights:     []float64{60, 40},
		Tags:        []string{"Food"},
		Link:        "https://example.com/receipt.jpg",
		Metadata: map[string]any{
			"category": "Groceries",
		},
	})
	require.NoError(err)
	require.NotEmpty(txn.ID)

	id = txn.ID

	// Update Transaction
	txn.Total = model.NewMoney(model.CAD, 14, 99)
	txn, err = st.UpsertTransaction(ctx, *txn)
	require.NoError(err)
	require.Equal(model.NewMoney(model.CAD, 14, 99), txn.Total)
	require.Equal(id, txn.ID)

	// Get Transaction
	txn = nil
	txn, err = st.GetTransaction(ctx, "not found")
	require.NoError(err)
	require.Nil(txn)
	txn, err = st.GetTransaction(ctx, id)
	require.NoError(err)
	require.NotNil(txn)
	require.Equal(txn.ID, id)

	// Delete Transaction
	err = st.DeleteTransaction(ctx, id)
	require.NoError(err)
	txn = nil
	txn, err = st.GetTransaction(ctx, id)
	require.NoError(err)
	require.Nil(txn)
}

func testStoreFind(require *require.Assertions, st store.Store) {
	ctx, cf := context.WithTimeout(context.Background(), 5*time.Second)
	defer cf()

	err := st.DropAll(ctx)
	require.NoError(err)

	rand.Seed(time.Now().UnixNano())

	// Create some test data
	const nTransactions = 10
	for i := 0; i < nTransactions; i++ {
		_, err := st.UpsertTransaction(ctx, model.Transaction{
			Project: fmt.Sprintf("Test Project %d", i%2),
			Date:    time.Date(2022, 02, 10+i, 0, 0, 0, 0, time.UTC),
		})
		require.NoError(err)
	}

	// Paginate the results of a query and assert that the results are filtered
	// and ordered as specified.
	const pageSize = 2
	var (
		lastDate = time.Now().UnixNano()
		page     []model.Transaction
		called   bool
		offset   int
	)
	for len(page) > 0 || !called {
		called = true

		// Find transactions in Test Project 0 and sort by date descending.
		page, err = st.FindTransactions(
			ctx,
			&model.Filter[model.TransactionField]{
				Keys: map[model.TransactionField]model.FilterValue{
					model.TransactionFieldProject: model.FilterValue{
						EQ: "Test Project 0",
					},
				},
			},
			[]model.FieldOrder[model.TransactionField]{
				{Field: model.TransactionFieldDate, Desc: true},
			},
			pageSize, offset,
		)

		require.NoError(err)

		require.True(len(page) <= pageSize)
		for _, txn := range page {
			offset++
			require.Equal("Test Project 0", txn.Project)
			require.True(txn.Date.UnixNano() <= lastDate)
			lastDate = txn.Date.UnixNano()
		}
	}
	require.False(called && offset < 1)
	require.Equal(5, offset)

	// Filter by date
	page, err = st.FindTransactions(
		ctx, &model.Filter[model.TransactionField]{
			Keys: map[model.TransactionField]model.FilterValue{
				model.TransactionFieldDate: model.FilterValue{
					EQ: time.Date(2022, 02, 10, 0, 0, 0, 0, time.UTC),
				},
			},
		},
		nil,
		100, 0,
	)
	require.NoError(err)
	require.Len(page, 1)
	require.Equal(10, page[0].Date.Day())

	// Create some test projects
	for i := 0; i < 3; i++ {
		_, err = st.UpsertProject(ctx, model.Project{
			Name: fmt.Sprintf("Test project %d", i),
		})
		require.NoError(err)
	}

	// List projects sorted by name desc
	projects, err := st.FindProjects(
		ctx, nil,
		[]model.FieldOrder[model.ProjectField]{{
			Field: model.ProjectFieldName,
			Desc:  true,
		}},
		10, 0,
	)
	require.NoError(err)
	require.Len(projects, 3)
	var lastName string
	for i, p := range projects {
		if i == 0 {
			lastName = p.Name
		} else {
			require.True(p.Name <= lastName, "%s should be <= %s", p.Name, lastName)
		}
	}
}

func TestMongoStoreCRUD(t *testing.T) {
	require := require.New(t)
	connectionURL := fixture.StartMongo()
	cx, cf := context.WithTimeout(context.Background(), 3*time.Second)
	mongoStore, err := store.NewMongoStore(cx, connectionURL, "testsplit", 0)
	require.NoError(err)
	cf()

	testStoreCRUD(require, mongoStore)
}

func TestMongoStoreFind(t *testing.T) {
	require := require.New(t)
	connectionURL := fixture.StartMongo()
	cx, cf := context.WithTimeout(context.Background(), 3*time.Second)
	mongoStore, err := store.NewMongoStore(cx, connectionURL, "testsplit", 0)
	require.NoError(err)
	cf()

	testStoreFind(require, mongoStore)
}
