package store_test

import (
	"os"
	"testing"

	"gitlab.com/zuern/kit/fixture"
)

func TestMain(m *testing.M) {
	code := m.Run()
	defer func() {
		fixture.Cleanup()
		os.Exit(code)
	}()
}
