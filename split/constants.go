package split

// Names of fields in structured log messages.
const (
	lfCorrelationID = "correlation-id"
	lfPanicMsg      = "panic-msg"
	lfRequestMethod = "request-method"
	lfRequestPath   = "request-path"
	lfStatusCode    = "status-code"
	lfUser          = "user"
)
