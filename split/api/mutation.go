package api

import (
	"context"

	"github.com/uhn/ggql/pkg/ggql"
	"gitlab.com/zuern/split/split/model"
)

type Mutation struct{}

// Resolve a field on an object. The field argument is the name of the
// field to resolve. The args parameter includes the values associated
// with the arguments provided by the caller. The function should return
// the field's object or an error. A return of nil is also possible.
func (m Mutation) Resolve(field *ggql.Field, args map[string]any) (data any, err error) {
	ctx, cx := mustGetContext(field.Context)
	defer func() {
		err = checkErr("Mutation."+field.Name, cx, err)
	}()
	err = model.NewErrorf(model.ERR_API, "%s is not a field on Mutation", field.Name)
	switch field.Name {
	case "createProject":
		data, err = m.upsertProject(ctx, cx, args, true)
	case "updateProject":
		data, err = m.upsertProject(ctx, cx, args, false)
	case "createTransaction":
		data, err = m.upsertTransaction(ctx, cx, args, true)
	case "updateTransaction":
		data, err = m.upsertTransaction(ctx, cx, args, false)
	}
	return data, err
}

func (m Mutation) upsertProject(ctx context.Context, cx *Context, args map[string]any, create bool) (data *model.Project, err error) {
	var input *model.Project
	if err = fromArgs(args, extractor{&input, "input", true}); err != nil {
		return
	}
	if create && input.ID != "" {
		return nil, model.NewErrorf(model.ERR_API, "id cannot be set for a create operation")
	} else if !create && input.ID == "" {
		return nil, model.NewErrorf(model.ERR_API, "id must be set for an update operation")
	}
	return cx.Store.UpsertProject(ctx, *input)
}

func (m Mutation) upsertTransaction(ctx context.Context, cx *Context, args map[string]any, create bool) (data *model.Transaction, err error) {
	var input *model.Transaction
	if err = fromArgs(args, extractor{&input, "input", true}); err != nil {
		return
	}
	if create && input.ID != "" {
		return nil, model.NewErrorf(model.ERR_API, "id cannot be set for a create operation")
	} else if !create && input.ID == "" {
		return nil, model.NewErrorf(model.ERR_API, "id must be set for an update operation")
	}
	return cx.Store.UpsertTransaction(ctx, *input)
}
