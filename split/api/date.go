package api

import (
	"time"

	"github.com/uhn/ggql/pkg/ggql"
	"gitlab.com/zuern/split/split/model"
)

type Date struct {
	ggql.Scalar
}

func NewDate() ggql.Type {
	return &Date{
		ggql.Scalar{
			Base: ggql.Base{
				N: "Date",
			},
		},
	}
}

const dateFmt = "2006-01-02"

// CoerceIn coerces an input value into the expected input type if possible
// otherwise an error is returned.
func (d *Date) CoerceIn(v interface{}) (data interface{}, err error) {
	switch tv := v.(type) {
	case string:
		var t time.Time
		if t, err = time.Parse(dateFmt, tv); err == nil {
			return t, nil
		}
	case time.Time:
		return tv, nil
	}
	return v, model.NewErrorf(model.ERR_API, "Date must be a non-null string in \"YYYY-MM-DD\" format")
}

// CoerceOut coerces a result value into a value suitable for output.
func (d *Date) CoerceOut(v interface{}) (interface{}, error) {
	if tv, ok := v.(time.Time); ok {
		return tv.Format(dateFmt), nil
	}
	return v, model.NewErrorf(model.ERR_API, "can not coerce a %T into a Date", v)
}
