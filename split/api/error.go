package api

import (
	"errors"

	"github.com/uhn/ggql/pkg/ggql"
	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/model"
)

var internalServerError = errors.New("internal server error")

// checkErr filters out internal errors from errors which should be returned to
// the client. If err is nil this function is a no-op. If err is not a
// model.Error then its message is logged and a generic "internal server error"
// message is returned to the user.
func checkErr(opName string, cx *Context, err error) error {
	if err == nil {
		return nil
	}
	e := &ggql.Error{
		Base: err,
		Extensions: map[string]any{
			"correlation_id": cx.CorrelationID,
		},
	}
	var m *model.Error
	if errors.As(err, &m) {
		cx.Log.WithOptions(log.CallerSkip(1)).Warn(opName+" error.", log.Err(err))
		e.Extensions["code"] = m.Code
	} else {
		cx.Log.WithOptions(log.CallerSkip(1)).Error(opName+" error.", log.Err(err))
		e.Base = internalServerError
	}
	return e
}
