package api

import (
	"context"

	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/model"
	"gitlab.com/zuern/split/split/store"
)

// Context is a request context.
type Context struct {
	// CorrelationID is a unique ID for the request context.
	CorrelationID string
	// Log is a logger for use within the scope of the Context.
	Log log.Logger
	// Store for data reading/writing.
	Store store.Store
	// User the current user if logged in.
	User *model.User
}

type _appCtx struct{}

// NewContext creates a context.Context from a Context.
func NewContext(ctx context.Context, c *Context) context.Context {
	return context.WithValue(ctx, _appCtx{}, c)
}

// FromContext extracts a Context from ctx or nil if not present.
func FromContext(ctx context.Context) *Context {
	cx, _ := ctx.Value(_appCtx{}).(*Context)
	return cx
}

// mustGetContext panics if the return values cannot be pulled from c.
func mustGetContext(c any) (context.Context, *Context) {
	ctx := c.(context.Context)
	cx := FromContext(ctx)
	if cx == nil {
		panic("Context was nil")
	}
	return ctx, cx
}
