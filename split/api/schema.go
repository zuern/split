package api

// Schema is the root of the GraphQL schema.
type Schema struct {
	Query    *Query
	Mutation *Mutation
}
