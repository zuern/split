package api

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/zuern/split/split/model"
)

type extractor struct {
	ptr     any
	key     string
	notNull bool
}

// fromArgs iterates the extractors and stores the value in args at key in the
// value pointed to by ptr. If notNull is true and the value is missing/nil, an
// Error is returned.
func fromArgs(args map[string]any, extractors ...extractor) error {
	for _, e := range extractors {
		av := args[e.key]
		if e.notNull && av == nil {
			return model.NewErrorf(model.ERR_API, "argument %s cannot be null", e.key)
		}
		if err := mapstructure.Decode(av, e.ptr); err != nil {
			return fmt.Errorf("fromArgs failed to decode GraphQL arg %q: %w", e.key, err)
		}
	}
	return nil
}
