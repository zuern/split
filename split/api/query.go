package api

import (
	"context"

	"github.com/uhn/ggql/pkg/ggql"
	"gitlab.com/zuern/split/split/model"
)

type Query struct{}

// Resolve a field on an object. The field argument is the name of the
// field to resolve. The args parameter includes the values associated
// with the arguments provided by the caller. The function should return
// the field's object or an error. A return of nil is also possible.
func (q Query) Resolve(field *ggql.Field, args map[string]any) (data any, err error) {
	ctx, cx := mustGetContext(field.Context)
	defer func() {
		err = checkErr("Query."+field.Name, cx, err)
	}()
	err = model.NewErrorf(model.ERR_API, "%s is not a field on Query", field.Name)
	switch field.Name {
	case "projects":
		data, err = q.projects(ctx, cx, args)
	case "transactions":
		data, err = q.transactions(ctx, cx, args)
	case "user":
		data, err = q.user(cx)
	}
	return data, err
}

func (q Query) projects(ctx context.Context, cx *Context, args map[string]any) (data any, err error) {
	data = []any{}
	var (
		first   = defaultPageSize
		offset  int
		filter  *model.Filter[model.ProjectField]
		orderBy []model.FieldOrder[model.ProjectField]
	)
	if err = fromArgs(
		args,
		extractor{&first, "first", false},
		extractor{&offset, "offset", false},
		extractor{&filter, "filter", false},
		extractor{&orderBy, "orderBy", false},
	); err != nil {
		return
	}
	page, err := cx.Store.FindProjects(
		ctx,
		filter,
		orderBy,
		first, offset,
	)
	if page == nil {
		return
	}
	return page, err
}

func (q Query) transactions(ctx context.Context, cx *Context, args map[string]any) (data any, err error) {
	data = []any{}
	var (
		first   = defaultPageSize
		offset  int
		filter  *model.Filter[model.TransactionField]
		orderBy []model.FieldOrder[model.TransactionField]
	)
	if err = fromArgs(
		args,
		extractor{&first, "first", false},
		extractor{&offset, "offset", false},
		extractor{&filter, "filter", false},
		extractor{&orderBy, "orderBy", false},
	); err != nil {
		return
	}
	page, err := cx.Store.FindTransactions(
		ctx,
		filter,
		orderBy,
		first, offset,
	)
	if page == nil {
		return
	}
	return page, err
}

func (q Query) user(cx *Context) (*model.User, error) {
	return cx.User, nil
}
