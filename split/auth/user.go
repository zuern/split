package auth

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/zuern/split/split/model"
)

type User struct {
	model.User
}

func (u *User) GetPID() (pid string) {
	return u.ID
}

func (u *User) PutPID(pid string) {
	u.ID = pid
}

func (u *User) GetPassword() string {
	return u.Password
}

func (u *User) PutPassword(password string) {
	u.Password = password
}

// GetArbitrary is used only to display the arbitrary data back to the user
// when the form is reset.
func (u *User) GetArbitrary() (arbitrary map[string]string) {
	if err := mapstructure.Decode(u, &arbitrary); err != nil {
		panic(err)
	}
	return arbitrary
}

// PutArbitrary allows arbitrary fields defined by the authboss library
// consumer to add fields to the user registration piece.
func (u *User) PutArbitrary(arbitrary map[string]string) {
	for k, v := range arbitrary {
		switch k {
		case FieldEmail:
			u.ID = v
		case FieldName:
			u.Name = v
		}
	}
}
