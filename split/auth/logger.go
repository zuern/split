package auth

import (
	"context"

	"github.com/volatiletech/authboss/v3"
	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/api"
)

type Logger struct {
	Log log.Logger
}

func (l *Logger) Info(s string) {
	l.Log.Info(s)
}

func (l *Logger) Error(s string) {
	l.Log.Error(s)
}

func (l *Logger) FromContext(ctx context.Context) authboss.Logger {
	eventType := log.Str("event-type", "auth")
	cx := api.FromContext(ctx)
	if cx != nil {
		return &Logger{Log: cx.Log.With(eventType)}
	}
	l.Log = l.Log.With(eventType)
	return l
}
