package auth

import (
	"context"
	"fmt"

	"github.com/volatiletech/authboss/v3"
	"gitlab.com/zuern/split/split/store"
)

// ServerStorer implements several authboss interfaces using a store.Store for
// persistence.
//
// Implements:
//     - authboss.CreatingServerStorer
type ServerStorer struct {
	Store store.Store
}

// Load will look up the user based on the passed the PrimaryID. Under
// normal circumstances this comes from GetPID() of the user.
//
// OAuth2 logins are special-cased to return an OAuth2 pid (combination of
// provider:oauth2uid), and therefore key be special cased in a Load()
// implementation to handle that form, use ParseOAuth2PID to see
// if key is an OAuth2PID or not.
func (s *ServerStorer) Load(ctx context.Context, key string) (authboss.User, error) {
	u, err := s.Store.GetUser(ctx, key)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, authboss.ErrUserNotFound
	}
	return &User{*u}, err
}

// Save persists the user in the database, this should never
// create a user and instead return ErrUserNotFound if the user
// does not exist.
func (s *ServerStorer) Save(ctx context.Context, user authboss.User) error {
	u := user.(*User)
	if u, err := s.Store.GetUser(ctx, user.GetPID()); err != nil {
		return fmt.Errorf("auth: save user: %w", err)
	} else if u == nil {
		return authboss.ErrUserNotFound
	}
	if _, err := s.Store.UpsertUser(ctx, u.User); err != nil {
		return fmt.Errorf("auth: save user: %w", err)
	}
	return nil
}

// New creates a blank user, it is not yet persisted in the database but is
// just for storing data.
func (s *ServerStorer) New(ctx context.Context) authboss.User {
	return &User{}
}

// Create the user in storage, it should not overwrite a user
// and should return ErrUserFound if it currently exists.
func (s *ServerStorer) Create(ctx context.Context, user authboss.User) error {
	if u, err := s.Store.GetUser(ctx, user.GetPID()); err != nil {
		return fmt.Errorf("auth: create user: %w", err)
	} else if u != nil {
		return authboss.ErrUserFound
	}
	_, err := s.Store.UpsertUser(ctx, user.(*User).User)
	return err
}
