// Copyright 2022 Kevin Zuern. All rights reserved.

package split

import (
	"io"
	"net/http"
	"os"
	"path"
	"strings"
)

// frontendHandler is an HTTP handler for serving a single page application.
// For a given request it returns the requested file if found in files
// otherwise it returns index.html.
type frontendHandler struct {
	files      http.FileSystem
	fileserver http.Handler
}

func newFrontendHandler(files http.FileSystem) *frontendHandler {
	fh := &frontendHandler{
		files:      files,
		fileserver: http.FileServer(files),
	}
	return fh
}

func (s *frontendHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	upath := req.URL.Path
	if !strings.HasPrefix(upath, "/") {
		upath = "/" + upath
	}
	upath = path.Clean(upath)
	f, err := s.files.Open(upath)
	if err != nil {
		if os.IsNotExist(err) {
			// ok, will return index.html page.
			f = nil
		} else if os.IsPermission(err) {
			http.Error(res, "403 Forbidden", http.StatusForbidden)
			return
		} else {
			http.Error(res, "500 Internal Server Error", http.StatusInternalServerError)
			return
		}
	}
	if f != nil {
		f.Close()
		s.fileserver.ServeHTTP(res, req)
		return
	}
	index, err := s.files.Open("/index.html")
	if err != nil {
		if os.IsNotExist(err) {
			http.Error(res, "404 Not Found", http.StatusNotFound)
		} else if os.IsPermission(err) {
			http.Error(res, "403 Forbidden", http.StatusForbidden)
		} else {
			http.Error(res, "500 Internal Server Error", http.StatusInternalServerError)
		}
		return
	}
	_, _ = io.Copy(res, index)
	index.Close()
}
