package util

import "encoding/json"

func ToJSON(x any, indent ...string) string {
	var i string
	if len(indent) > 0 {
		i = indent[0]
	}
	bytes, err := json.MarshalIndent(x, "", i)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}
