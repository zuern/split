package split

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/zuern/kit/log"
	"gitlab.com/zuern/split/split/api"
)

// addContext adds required context to the request. Adds reference to current
// user, store, and a logger with correlation id.
func (s *Server) addContextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		cx := api.FromContext(req.Context())
		if cx == nil {
			cx = &api.Context{
				Store:         s.store,
				Log:           s.log,
				CorrelationID: uuid.NewString(),
			}
			cx.Log = cx.Log.With(log.Str(lfCorrelationID, cx.CorrelationID))
			res.Header().Set(lfCorrelationID, cx.CorrelationID)

			user, err := s.currentUser(req)
			if err != nil {
				cx.Log.Error("failed to look up user", log.Err(err))
				http.Error(res, http.StatusText(500), 500)
				return
			}
			if user != nil {
				cx.User = &user.User
				cx.Log = cx.Log.With(log.Str(lfUser, cx.User.ID))
			}
		}
		next.ServeHTTP(res, req.WithContext(api.NewContext(req.Context(), cx)))
	})
}

// logResponseMiddleware will record the response to a request along with
// request details. If 5xx error occurs it logs at Error level otherwise Debug
// level. If a panic occurred it returns 500 and logs the panic and stack
// trace.
func (s *Server) logResponseMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		// Capture status code with custom response writer and log it.
		rw := &responseWriter{res, 0}
		defer func() {
			fields := []log.Field{
				log.Str(lfRequestMethod, req.Method),
				log.Str(lfRequestPath, req.URL.Path),
			}
			cx := api.FromContext(req.Context())
			if r := recover(); r != nil {
				http.Error(rw, http.StatusText(500), 500)
				fields = append(fields, log.Int(lfStatusCode, int64(rw.statusCode)))

				fields = append(fields, log.Str(lfPanicMsg, fmt.Sprintf("%v", r)))
				cx.Log.DPanic("Handled request and panicked", fields...)
				return
			}
			fields = append(fields, log.Int(lfStatusCode, int64(rw.statusCode)))
			if rw.statusCode >= 500 && rw.statusCode <= 599 {
				cx.Log.Error("Handled request with errors", fields...)
			} else {
				cx.Log.Debug("Handled request", fields...)
			}
		}()
		next.ServeHTTP(rw, req)
	})
}
