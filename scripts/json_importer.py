import argparse
import datetime
import json
import requests

from typing import Dict, Union

ID = "id"
PROJECT = "project"
DATE = "date"
DESC = "description"
NOTES = "notes"
TOTAL = "total"
PAYER = "payer"
OWERS = "owers"
WEIGHTS = "weights"
TAGS = "tags"
LINK = "link"
METADATA = "metadata"


class JSONImporter:
    def __init__(
        self,
        project_name: str,
        project_id: Union[str, None],
        convert,
        api_url: str,
        json_file_path: str,
    ):
        self.project_name = project_name
        self.project_id = project_id
        self.convert = convert
        self.api_url = api_url
        self.json_file_path = json_file_path
        return

    def do_import(self):
        if self.project_id is None:
            # Create the project.
            data = self.executeMutation(
                f"""
mutation{{
    createProject(input:{{
        name:"{self.project_name}"}}
    ){{
        id
}}}}"""
            )
            self.project_id = data["createProject"]["id"]

        source = None
        with open(self.json_file_path, "r") as f:
            source = json.load(f)
        for x in source:
            txn = self.convert(x)
            txn[PROJECT] = self.project_id
            print(txn)
            data = self.executeMutation(
                f"""
mutation{{
    createTransaction(input:{{
        date: "{txn[DATE].strip()}",
        description: "{txn[DESC].strip()}",
        total: {{
            currency: {txn[TOTAL]['currency'].strip()},
            millicents: {txn[TOTAL]['millicents']},
        }},
        payer: "{txn[PAYER].strip()}",
        owers: {json.dumps([x.strip() for x in txn[OWERS]])},
        project: "{self.project_id}"
    }}){{
        id
    }}
}}
"""
            )
        return

    def executeMutation(self, mutation):
        print("======================================================")
        print(mutation)
        print("------------------------------------------------------")
        resp = requests.post(url=self.api_url, data=mutation.encode("utf-8"))
        if resp.status_code != 200:
            raise Exception(f"HTTP {resp.status_code}: {resp.text}")
        resp = resp.json()
        print(resp)
        print("======================================================")
        if "errors" in resp and len(resp["errors"]) > 0:
            raise Exception("graphql errors occurred: " + json.dumps(resp["errors"]))
        return resp["data"]


class Money:
    def __init__(self, currency: str, millicents: int):
        self.currency = currency
        self.millicents = millicents


def _float_to_millicents(dollars: float) -> int:
    return round(dollars * 100000)


def _convert(obj: Dict) -> Dict:
    return {
        DATE: obj[DATE],
        DESC: obj["what"],
        TOTAL: {"currency": "CAD", "millicents": _float_to_millicents(obj["amount"])},
        PAYER: obj["payer_name"],
        OWERS: obj["owers"],
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--url",
        help="endpoint of the graphql api",
        default="http://localhost:3000/api/graphql",
    )
    parser.add_argument(
        "--project",
        help="name of the new project to create to track the imported transactions",
        default=f"Imported transactions {datetime.datetime.now().isoformat()}",
    )
    parser.add_argument(
        "--project_id",
        help="id of the existing project to use to track the imported expenses",
    )
    parser.add_argument("file", help="path to json file to import transactions from")
    args = parser.parse_args()
    importer = JSONImporter(
        args.project,
        args.project_id,
        _convert,  # <-- function to convert incoming json into split Transaction
        args.url,
        args.file,
    )
    importer.do_import()
