LINT_VERSION=v1.45.0

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

all: help

## Setup:
init: ## Install linters, packages, and dependencies required for the project.
ifneq '$(LINT_VERSION)' '$(shell golangci-lint --version 2>/dev/null | cut -d " " -f 4 -)'
	@echo Getting golangci-lint $(LINT_VERSION)
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@$(LINT_VERSION)
else
endif
	$(MAKE) -C split/frontend init

## Development:

lint: lint-go ## Run all available linters

lint-go: ## Run go-specific lint
	golangci-lint run ./...

run: ## Run the server
	go run . -dev -lvl debug

## Testing:
test: test-go ## Run all available tests

test-go: ## Run go-specific tests.
	go test ./...

coverage: ## Run the tests and report on code coverage.
	go test -cover -covermode=count -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

## Help:
help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)
